package com.gmail.at.andrewmove.avalon;

import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

@RestController
public class AvalonController {

    private final Map<String, String> userCards = new ConcurrentHashMap<>();

    private final Map<String, Boolean> userToVote = new ConcurrentHashMap<>();

    private final Queue<String> cardDeck = new ConcurrentLinkedQueue<>();

    private volatile boolean votingLocked = true;

    @GetMapping("cards")
    public Map<String, String> getPlayerCard(@RequestParam String userId) {
        String card = userCards.computeIfAbsent(userId, key -> cardDeck.poll());
        HashMap<String, String> result = new HashMap<>();
        result.put("card", card);
        return result;
    }

    @PostMapping("cards")
    public void postCards(@RequestBody List<String> cards) {
        Collections.shuffle(cards);
        cardDeck.clear();
        cardDeck.addAll(cards);
    }

    @PostMapping("votes")
    public void postVote(@RequestParam String userId, @RequestParam boolean vote) {
        if (votingLocked) return;
        userToVote.put(userId, vote);
    }

    @GetMapping("votes")
    public List<Boolean> getVotes() {
        return new ArrayList<>(userToVote.values());
    }

    @DeleteMapping("votes")
    public void deleteVotes() {
        userToVote.clear();
        votingLocked = false;
    }

    @PostMapping("endVoting")
    public void endVoting() {
        votingLocked = true;
    }

}